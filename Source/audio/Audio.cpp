/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"
#include <cmath>

Audio::Audio() 
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs

    for (int i = 0; i < 12; i++) {
        oscillator[i] = &sinOscillator[i];
    }
    
//    fc = 5000;
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);

    octaveMultiplier = 1;
    sample = 0.f;
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        
        myFilter->setBiquad(filterType, cutoffFrequency / 44100., 0.707, 2);
        hpFilter->setBiquad(highpass, 100. / 44100., 0.707, 0);

        sample = oscillator[0]->getSample();
        
        for(int i = 1; i < 12 ; i++)
            sample += oscillator[i]->getSample();
    
        //apply filter to input samples
        sample = myFilter->process(hpFilter->process(sample));
       
        *outL = sample;
        *outR = sample;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
}

void Audio::audioDeviceStopped()
{

}

void Audio::beep(int i, float Multiplier)
{
    octaveMultiplier = Multiplier;
    
    switch(i){
        case 0:
        {
            DBG("i == " << i);
            oscillator[0]->setFrequency(261.63 * octaveMultiplier);
            oscillator[0]->setAmplitude(0.5);
            break;
        }
        case 1:
        {
            DBG("i == " << i);
            oscillator[1]->setFrequency(277.18 * octaveMultiplier);
            oscillator[1]->setAmplitude(0.5);
            break;
        }
        case 2:
        {
            DBG("i == " << i);
            oscillator[2]->setFrequency(293.66 * octaveMultiplier);
            oscillator[2]->setAmplitude(0.5);
            break;
        }
        case 3:
        {
            DBG("i == " << i);
            oscillator[3]->setFrequency(311.13 * octaveMultiplier );
            oscillator[3]->setAmplitude(0.5);
            break;
        }
        case 4:
        {
            DBG("i == " << i);
            oscillator[4]->setFrequency(329.63 * octaveMultiplier);
            oscillator[4]->setAmplitude(0.5);
            break;
        }
        case 5:
        {
            DBG("i == " << i);
            oscillator[5]->setFrequency(349.23 * octaveMultiplier);
            oscillator[5]->setAmplitude(0.5);
            break;
        }
        case 6:
        {
            DBG("i == " << i);
            oscillator[6]->setFrequency(369.99 * octaveMultiplier);
            oscillator[6]->setAmplitude(0.5);
            break;
        }
        case 7:
        {
            DBG("i == " << i);
            oscillator[7]->setFrequency(392.00 * octaveMultiplier);
            oscillator[7]->setAmplitude(0.5);
            break;
        }
        case 8:
        {
            DBG("i == " << i);
            oscillator[8]->setFrequency(415.30 * octaveMultiplier);
            oscillator[8]->setAmplitude(0.5);
            break;
        }
        case 9:
        {
            DBG("i == " << i);
            oscillator[9]->setFrequency(440.00 * octaveMultiplier);
            oscillator[9]->setAmplitude(0.5);
            break;
        }
        case 10:
        {
            DBG("i == " << i);
            oscillator[10]->setFrequency(466.16 * octaveMultiplier);
            oscillator[10]->setAmplitude(0.5);
            break;
        }
        case 11:
        {
            DBG("i == " << i);
            oscillator[11]->setFrequency(493.88 * octaveMultiplier);
            oscillator[11]->setAmplitude(0.5);
            break;
        }
    }
}

void Audio::beepStop(int i)
{
    oscillator[i]->stop();
}

void Audio::setWaveform(int state)
{
    if (state == 1)
    {
        for (int i = 0; i < 12; i++)
            oscillator[i] = &sinOscillator[i];
    }
    else if(state == 2)
    {
        for (int i = 0; i < 12; i++)
            oscillator[i] = &squareOscillator[i];
    }
    else if(state == 3)
    {
        for (int i = 0; i < 12; i++)
           oscillator[i] = &sawOscillator[i];
    }
}

void Audio::setFilterCutoff(float cutoff)
{
    cutoffFrequency = cutoff;
}

void Audio::setFilterType(int type)
{
    filterType = type;
}

bool Audio::tailOff()
{
    return true;
}