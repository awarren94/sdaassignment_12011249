//
//  Filter.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 28/12/2015.
//
//


//
//  Biquad.h
//
//  Created by Nigel Redmon on 11/24/12
//  EarLevel Engineering: earlevel.com
//  Copyright 2012 Nigel Redmon
//
//  For a complete explanation of the Biquad code:
//  http://www.earlevel.com/main/2012/11/26/biquad-c-source-code/
//
//  License:
//
//  This source code is provided as is, without warranty.
//  You may copy and distribute verbatim copies of this document.
//  You may modify and use this source code to create binary code
//  for your own purposes, free or commercial.
//

#ifndef FILTER_H_INCLUDED
#define FILTER_H_INCLUDED

enum {
    lowpass = 0,
    highpass,
};

class Filter {
public:
    Filter();
    Filter(int type, double Fc, double Q, double peakGainDB);
    ~Filter();
    void setType(int type);
    void setQ(double Q);
    void setFc(double Fc);
    void setPeakGain(double peakGainDB);
    void setBiquad(int type, double Fc, double Q, double peakGain);
    float process(float in);
    
protected:
    void calcBiquad(void);
    
    int type;
    double a0, a1, a2, b1, b2;
    double Fc, Q, peakGain;
    double z1, z2;
};



#endif /* FILTER_H_INCLUDED */