/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SequencerButtonsUI.h"
#include "SinOscillator.h"
#include "SquareOscillator.h"
#include "SawOscillator.h"
#include "OctaveButtons.h"
#include "Filter.h"
#include "Oscillator.h"


class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback,
                public Counter::Listener,
                public Filter,
                public Synthesiser
{
    
public:
    /** 
    Constructor 
    */
    Audio();
    
    /**
    Destructor 
    */
    ~Audio();
    
    /** 
    Returns the audio device manager, don't keep a copy of it! 
    */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void counterChanged (uint64 counterValue) override
    {
        DBG("Audio::" << counterValue);
    }
    
    //start note
    void beep (int i, float Multiplier);
    
    //stop note
    void beepStop (int i);
    
    //set oscillator type
    void setWaveform(int state);
    
    void setFilterCutoff (float);
    
    void setFilterType (int);
    
    bool tailOff();
    
     //   float filter (float);
private:
    AudioDeviceManager audioDeviceManager;
    SequencerButtonsUI iterate;
   
    SinOscillator sinOscillator[12];
    SquareOscillator squareOscillator[12];
    SawOscillator sawOscillator[12];
  
    Filter *myFilter = new Filter();
    Filter *hpFilter = new Filter();// create a Biquad, lpFilter;
    
    
    float cutoffFrequency;
    int filterType;
    float amplitude;
  
    Oscillator* oscillator[12];
   
    float sample;
    float frequency;
    Counter counter;
    
   // OctaveButtons octaveButtons;
    float octaveMultiplier;
};



#endif  // AUDIO_H_INCLUDED
