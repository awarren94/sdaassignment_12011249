//
//  OctaveButtons.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 18/12/2015.
//
//

#ifndef OCTAVEBUTTONS_H_INCLUDED
#define OCTAVEBUTTONS_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"



class OctaveButtons : public Component,
                      public Button::Listener
{
public:
    OctaveButtons();
    
    ~OctaveButtons();
    
    void resized();
    
    void buttonClicked (Button* button);
 
    float getOctaveMultiplier();
    
    void setOctaveMultiplier(float multiplier);
private:
    
    ToggleButton octaveButtons[5];
    float octaveMultiplier;
    
    
};

#endif /* OCTAVEBUTTONS_H_INCLUDED */
