/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include <math.h>



//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    for(int i = 0; i < amountOfRows; i++)
    {
        addAndMakeVisible(sequencerButtonsUi[i]);
    }
  
    counter.addListener(this);

    setSize (900 , 500);
    addAndMakeVisible(&startButton);
    startButton.addListener(this);
   
    addAndMakeVisible(&bpmSlider);
    bpmSlider.addListener(this);
    bpmSlider.setRange(60, 170, 1.);
    bpmSlider.setSliderStyle(Slider::LinearBar);
    bpmSlider.setColour(Slider::thumbColourId  , Colours::darkred);
    bpmSlider.setColour(Slider::backgroundColourId  , Colours::black);
    bpmSlider.setColour(Slider::textBoxTextColourId    , Colours::lightgrey);
    bpmSlider.setColour(Slider::textBoxOutlineColourId    , Colours::black);
    bpmSlider.setValue(120);
    
    addAndMakeVisible(&clearButton);
    clearButton.addListener(this);
    clearButton.setButtonText("CLEAR");
    clearButton.setColour(TextButton::buttonColourId, Colours::darkred);
    clearButton.setColour(TextButton::textColourOffId, Colours::lightgrey);
 
    addAndMakeVisible(&oscillatorBox);
    oscillatorBox.addListener(this);
    oscillatorBox.addItem("sin", 1);
    oscillatorBox.addItem("square", 2);
    oscillatorBox.addItem("saw", 3);
    oscillatorBox.setSelectedId(1);
    oscillatorBox.setColour(ComboBox::backgroundColourId, Colours::black);
    oscillatorBox.setColour(ComboBox::arrowColourId , Colours::darkred);
    oscillatorBox.setColour(ComboBox::outlineColourId , Colours::black);
    oscillatorBox.setColour(ComboBox::buttonColourId , Colours::black);
    oscillatorBox.setColour(ComboBox::textColourId, Colours::lightgrey);
    oscillatorBox.setJustificationType(juce::Justification::horizontallyCentred);
    
    
    addAndMakeVisible(&filterTypeBox);
    filterTypeBox.addListener(this);
    filterTypeBox.addItem("lowpass", 1);
    filterTypeBox.addItem("highpass", 2);
    filterTypeBox.setSelectedId(1);
    filterTypeBox.setColour(ComboBox::backgroundColourId, Colours::black);
    filterTypeBox.setColour(ComboBox::arrowColourId , Colours::darkred);
    filterTypeBox.setColour(ComboBox::outlineColourId , Colours::black);
    filterTypeBox.setColour(ComboBox::buttonColourId , Colours::black);
    filterTypeBox.setColour(ComboBox::textColourId, Colours::lightgrey);
    filterTypeBox.setJustificationType(juce::Justification::horizontallyCentred);
    
    
    addAndMakeVisible(&cutoffSlider);
    cutoffSlider.addListener(this);
    cutoffSlider.setRange(0., 1.);
    cutoffSlider.setValue(0.9);
    cutoffSlider.setTextBoxStyle(Slider::NoTextBox, true, true, true);
    cutoffSlider.setSliderStyle(Slider::LinearBar);
    cutoffSlider.setColour(Slider::thumbColourId  , Colours::darkred);
    cutoffSlider.setColour(Slider::backgroundColourId  , Colours::black);
    cutoffSlider.setColour(Slider::textBoxTextColourId    , Colours::lightgrey);
    cutoffSlider.setColour(Slider::textBoxOutlineColourId    , Colours::black);
    
    //addAndMakeVisible(&swingSlider);
    swingSlider.addListener(this);
    swingSlider.setRange(1, 5, 1);
    
    addAndMakeVisible(&uiGraphics);
    uiGraphics.toBack();
    
    addAndMakeVisible(&octaveButtonsUi);

    startTimer(20);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{

    for(int i = 0; i < amountOfRows; i++)
    {
        
        sequencerButtonsUi[i].setBounds(getWidth()/(getWidth()/15),
                                        (i * (getHeight() - (getHeight()/4.6))/ 12 + (getHeight()/15) ),
                                        getWidth() - getWidth()/4 ,
                                        (getHeight() - (getHeight()/300))/12);
    }
    
        
   
//    sequencerRowsGui.setBounds(getWidth()/20, getHeight() / 4, getWidth()/2, getHeight()/2);
    
    startButton.setBounds(getWidth() / 1.16, getHeight()/ 6.9, getWidth()/15, getHeight()/15);
    
    bpmSlider.setBounds(getWidth() / 1.285, getHeight()/1.517, getWidth()/5 , getHeight()/15);
                            
    clearButton.setBounds(getWidth() / 1.285, getHeight()/1.35, getWidth()/5, getHeight()/10);
    
    oscillatorBox.setBounds(getWidth() / 1.285, getHeight()/3.8, getWidth()/5, getHeight()/15);
    
    filterTypeBox.setBounds(getWidth() / 1.285, getHeight()/2.55, getWidth()/5, getHeight()/15);
    
    cutoffSlider.setBounds(getWidth() / 1.285, getHeight()/1.88, getWidth()/5. , getHeight()/15);
 
    octaveButtonsUi.setBounds(getWidth()/11, getHeight() - getHeight()/6.5, getWidth() - getWidth()/50, getHeight()/10);
    
    swingSlider.setBounds(getWidth() / 1.285, getHeight()/1.1, getWidth()/5, getHeight()/15);
    
    uiGraphics.setBounds(0, 0, getWidth(), getHeight());
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}


void MainComponent::buttonClicked(Button* button)
{
    if(button == &startButton)
    {
        if(startButton.getToggleState() == true)
        {
            counter.start();
            DBG("START");
            startTimer(20);
        }
        
        else if (startButton.getToggleState() == false)
        {
            DBG("STOP");
            for(int i = 0; i < 12; i++)
            {
                audio.beepStop(i);
                sequencerButtonsUi[i].resetStepPosition();
            }
            stopTimer();
            counter.stop();
        }
    }
    
    else if(button == &clearButton)
    {
        for(int i = 0; i < 12; i++)
        {
            sequencerButtonsUi[i].clearButtons();
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if(slider == &bpmSlider)
    {
        counter.setInterval(bpmSlider.getValue());
    }
    
    else if(slider == &swingSlider)
    {
        oddSwingAmount = 1 - swingSlider.getValue();
        evenSwingAmount = swingSlider.getValue() + 1;
      //  DBG("\n\n\n\n\n\n swing amaount = \n\n\n\n" << oddSwingAmount);
        counter.setSwingValue(swingSlider.getValue());
    }
    
    else if(slider == &cutoffSlider)
    {
        float cutoff = cutoffSlider.getValue();
        cutoff = pow(cutoff, 3);
        cutoff = (cutoff * 15000) + 60;
        audio.setFilterCutoff(cutoff);
        DBG(cutoff);
    }
}

void MainComponent::comboBoxChanged(ComboBox* comboBox )
{
    if(comboBox == &oscillatorBox)
    {
        DBG(oscillatorBox. getSelectedId());
        if(oscillatorBox.getSelectedId() == 1)
        {
            DBG("SINE");
            audio.setWaveform (1);
        }
        
        else if(oscillatorBox.getSelectedId() == 2)
        {
            DBG("SQUARE");
            audio.setWaveform(2);
            
        }
        
        else if(oscillatorBox.getSelectedId() == 3)
        {
            DBG("SAW");
            audio.setWaveform(3);
        }
    }
    
    else if(comboBox == &filterTypeBox)
    {
        if(filterTypeBox.getSelectedId() == 1)
        {
            audio.setFilterType(0);
        }
        else if(filterTypeBox.getSelectedId() == 2)
        {
            audio.setFilterType(1);
        }
    }
}

void MainComponent::counterChanged (uint64 counterValue)
{
    DBG("MainComponent::" << counterValue);
    counterStep = counterValue;
}

void MainComponent::timerCallback()
{
    //iterate through sequencer buttons
    for (int i = 0; i < amountOfRows; i++)
    {
        if(getButtonState() == true)
        {
            sequencerButtonsUi[i].highlightStep (counterStep);
        
            if(sequencerButtonsUi[i].getButtonState(counterStep) == true)
            {
                audio.beep(i, octaveButtonsUi.getOctaveMultiplier() );
            }
            else if (sequencerButtonsUi[i].getButtonState(counterStep) == false)
            {
                audio.beepStop(i);
            }
        }
        
        else
        {
            break;
        }
    }
}

bool MainComponent::getButtonState()
{
    return startButton.getToggleState();
}


