//
//  OctaveButtons.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 18/12/2015.
//
//

#include "OctaveButtons.h"


OctaveButtons::OctaveButtons()
{
    for(int i = 0; i < 5; i++)
    {
        addAndMakeVisible(&octaveButtons[i]);
        octaveButtons[i].addListener(this);
    }
    octaveButtons[2].setToggleState(true, dontSendNotification);
    setOctaveMultiplier(1);
}

OctaveButtons::~OctaveButtons()
{
}

void OctaveButtons::resized()
{
    for (int i = 0; i < 5; i++)
    {
        octaveButtons[i].setBounds((i * getWidth())/5, getHeight()/3, (getWidth()/5) - (getWidth()/8), getHeight ()/1.5);
    }
    
}

void OctaveButtons::buttonClicked(Button* button)
{
    for(int i = 0; i < 5; i++)
    {
    if(button == &octaveButtons[i])
    {
        if(octaveButtons[i].getToggleState() == true)
        {
            if(i == 0)
            {
                setOctaveMultiplier(0.25);
                for(int c = 1; c < 5; c++)
                {
                    octaveButtons[c].setToggleState(false, dontSendNotification);
                }
            }
            else if (i == 1)
            {
                setOctaveMultiplier(0.5);
                int c = 0;

                do
                {
                    if(c == 1)
                    {   // skip the iteration.
                        c++;
                        continue;
                    }
                    octaveButtons[c].setToggleState(false, dontSendNotification);
                    c++;
                }while(c < 5);
            }
            else if (i == 2)
            {
                setOctaveMultiplier(1.);
                int c = 0;
                
                do
                {
                    if(c == 2)
                    {   // skip the iteration.
                        c++;
                        continue;
                    }
                  
                    octaveButtons[c].setToggleState(false, dontSendNotification);
                    c++;
                }while(c < 5);

            }
            else if (i == 3)
            {
                setOctaveMultiplier(2.);
                int c = 0;
                
                do
                {
                    if(c == 3)
                    {   // skip the iteration
                        c++;
                        continue;
                    }
               
                    octaveButtons[c].setToggleState(false, dontSendNotification);
                    c++;
                }while(c < 5);

            }
            else if (i == 4)
            {
                setOctaveMultiplier(3.);
                int c = 0;
                
                do
                {
                    if(c == 4)
                    {   // skip the iteration
                        c++;
                        continue;
                    }
                    octaveButtons[c].setToggleState(false, dontSendNotification);
                    c++;
                }while(c < 5);
            }
        }
    }
    }
}

void OctaveButtons::setOctaveMultiplier(float multiplier)
{
    octaveMultiplier = multiplier;
    
    //return octaveMultiplier;
}


float OctaveButtons::getOctaveMultiplier()
{
    return octaveMultiplier;
}