//
//  SequencerButtonsUI.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//

#include "SequencerButtonsUI.h"
#include "stdio.h"


SequencerButtonsUI::SequencerButtonsUI()
{
    counterPosition = 0;
    
    for(int i = 0; i < amountOfSteps; i++)
    {
       
        addAndMakeVisible(&onButton[i]);
        if(i == 0 || i == 4 || i == 8 || i == 12)
        {
        onButton[i].setColour(TextButton::buttonColourId, Colours::slategrey);
        }
        else
        {
        onButton[i].setColour(TextButton::buttonColourId, Colours::black);

        }
        onButton[i].setColour(TextButton::buttonOnColourId, Colours::yellow);
        onButton[i].addListener(this);
        
        
    }
    //interval = 1;
    
    c = 0;
    }

SequencerButtonsUI::~SequencerButtonsUI()
{

}

void SequencerButtonsUI::resized()
{
    
    for(int i = 0; i < amountOfSteps; i++)
    {
       
       onButton[i].setBounds((i * getWidth())/amountOfSteps, getHeight()/3, (getWidth()/amountOfSteps) - (getWidth()/200), getHeight ()/1.5);
        
    }
    
    
}

//Button Listener
void SequencerButtonsUI::buttonClicked (Button* button)
{
    for(int i = 0; i < amountOfSteps; i++)
    {
        if(button == &onButton[i])
        {
            if(onButton[i].getToggleState() == true)
            {
        
            onButton[i].setToggleState(false, dontSendNotification);
             DBG(onButton[i].getToggleState());
            }
            else if (onButton[i].getToggleState() == false)
            {
                onButton[i].setToggleState(true, dontSendNotification);
                DBG(onButton[i].getToggleState());
            }
    }
    
    }
}

bool SequencerButtonsUI::getButtonState(int i)
{
    return onButton[i].getToggleState();
}

const int SequencerButtonsUI::getAmountOfSteps()
{
    return amountOfSteps;
}

void SequencerButtonsUI::resetStepPosition()
{
    for(int i = 0 ; i < 16 ; i++)
    {
    onButton[i].setColour(TextButton::buttonColourId, Colours::black);
      
    }
}

void SequencerButtonsUI::clearButtons()
{
    
    for(int i = 0 ; i < 16 ; i++)
    {
        
          onButton[i].setToggleState(false, dontSendNotification);
        
    }

    
}


