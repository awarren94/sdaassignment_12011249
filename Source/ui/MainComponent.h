/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "Oscillator.h"
#include "OctaveButtons.h"
#include "SequencerButtonsUi.h"
#include "Filter.h"
#include "UiGraphics.h"
//#include "SequencerRowsGui.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Counter::Listener,
                        public Timer,
                        public Button::Listener,
                        public Slider::Listener,
                        public ComboBox::Listener

{
public:
    //==============================================================================
    /** 
    Constructor
    */
    MainComponent (Audio& audio_);

    /** 
    Destructor 
    */
    ~MainComponent();

    void resized() override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    void buttonClicked (Button* button) override;
    
    void sliderValueChanged (Slider* slider) override;
    
    void comboBoxChanged(ComboBox* comboBox ) override;

    bool getButtonState();
    
    void counterChanged (uint64 counterValue) override;
    
    void timerCallback();
    
    
private:
    Audio& audio;
    
    const static int amountOfRows = 12;
    int counterStep;
    int oddSwingAmount, evenSwingAmount;
    
    SequencerButtonsUI sequencerButtonsUi[amountOfRows];
    ToggleButton startButton;
    Slider bpmSlider;
    Slider swingSlider;
    TextButton clearButton;
    ComboBox oscillatorBox;
   
    ComboBox filterTypeBox;
    Slider cutoffSlider;
    Filter filter;
    Label octaveLabel;

    Audio vAudio;
    OctaveButtons octaveButtonsUi;
    Counter counter;
    UiGraphics uiGraphics;

    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
