//
//  UiGraphics.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/01/2016.
//
//

#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

/**
Class for UI graphics
*/

class UiGraphics : public Component
{
public:
   
    UiGraphics();
    
    ~UiGraphics();
    
    void paint (Graphics& g);
   
private:

};

#endif /* GRAPHICS_H_INCLUDED */
