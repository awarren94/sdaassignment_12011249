//
//  SequencerButtonsUI.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//

#ifndef SEQUENCERBUTTONSUI_H_INCLUDED
#define SEQUENCERBUTTONSUI_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Counter.h"

class SequencerButtonsUI :  public Component,
                            public Button::Listener


{
public:

    SequencerButtonsUI();
    
    ~SequencerButtonsUI();
    
    void resized();
    
    //Button Listener
    void buttonClicked (Button* button);
    
 //   void iterate();
    
   const int getAmountOfSteps();
    
    //void counterChanged (uint64 counterValue) override;

    void highlightStep(int step)
    {
        if(step == 0)
        {
             onButton[15].setColour(TextButton::buttonColourId, Colours::black);
        }
        
        else if(step > 0)
        {
            onButton[step-1].setColour(TextButton::buttonColourId, Colours::black);
            
        }
        
       
        onButton[step].setColour(TextButton::buttonColourId, Colours::grey);

        if(step == 0 || step == 4 || step == 8 || step == 12)
        {
            onButton[step].setColour(TextButton::buttonColourId, Colours::transparentBlack);
        }
    
    }
    
    bool getButtonState(int i);
    
    void resetStepPosition();
    
    void clearButtons();
    
private:

    static const int amountOfSteps = 16;
    TextButton onButton[amountOfSteps];
    //Counter counter;
   // uint32 interval;
    ToggleButton startButton;
    juce::ArrowButton::ButtonState buttonDown;
    
    int c ;
    Atomic<int> counterPosition;
    
    
};



#endif /* SEQUENCERBUTTONSUI_H_INCLUDED */
