//
//  UiGraphics.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/01/2016.
//
//

#include "UiGraphics.h"

UiGraphics::UiGraphics()
{

}

UiGraphics::~UiGraphics()
{

}

void UiGraphics::paint (Graphics& g)
{    
    g.setFont (Font ("Helvetica Neue", getHeight()/20, Font::plain));
    g.setColour(Colours::grey);
    g.drawText ("-2", getWidth()/11, getHeight()/10, getWidth()
                , getHeight() + getHeight() / 1.4, true);
    g.drawText ("-1", getWidth()/3.5, getHeight()/10, getWidth(), getHeight() + getHeight() / 1.4, true);
    g.drawText ("0", getWidth()/2.05
                , getHeight()/10, getWidth(), getHeight() + getHeight() / 1.4, true);
    g.drawText ("+1", getWidth()/1.48, getHeight()/10, getWidth(), getHeight() + getHeight() / 1.4, true);
    g.drawText ("+2", getWidth()/1.15, getHeight()/10, getWidth(), getHeight() + getHeight() / 1.4, true);
    
    g.setFont(getHeight()/16);
    
    g.drawText("START / STOP", getWidth() - getWidth()/4.5, getHeight()/14, getWidth() / 5, getHeight()/10, Justification::centred);
    g.drawText("BPM", getWidth() - getWidth()/4.5, getHeight()/1.715, getWidth() / 5, getHeight()/10, Justification::centred);
    g.drawText("OSCILLATOR", getWidth() - getWidth()/4.50, getHeight()/5.5, getWidth() / 5, getHeight()/10, Justification::centred);
    g.drawText("FILTER TYPE", getWidth() - getWidth()/4.5, getHeight()/3.15, getWidth() / 5, getHeight()/10, Justification::centred);
    g.drawText("CUTOFF", getWidth() - getWidth()/4.5, getHeight()/2.2, getWidth() / 5, getHeight()/10, Justification::centred);
    
    Graphics& h = g;
    
    h.setColour(juce::Colour::fromRGBA (50,64,64,120));
    
    h.fillRect(getWidth()/(getWidth()/10), getHeight() - (getHeight()/1.07)
               ,
               getWidth() - getWidth()/(getWidth()/20) ,
               (getHeight() - (getHeight()/5)));
    
    Graphics& i = h;
    
    i.setColour(Colours::black);
    i.drawRect(getWidth() - getWidth()/4.35, getHeight()/13, getWidth()/4.65, getHeight()/1.289, 3);
  
}


