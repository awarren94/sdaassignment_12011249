//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//


#include "Counter.h"
#include "JuceHeader.h"



Counter::Counter() : Thread ("CounterThread")
{
    counterValue = 0;
    interval = 20;
   
    //start();
}

Counter::~Counter()
{
    stop();
}

void Counter::run()
{
    while (!threadShouldExit())
    {
       
            uint32 currentTime = Time::getMillisecondCounter();

//        if(playState == true)
//        {
            listenerList.call(&Listener::counterChanged, counterValue++);
            
        
            if (counterValue == 16)
                counterValue = 0;
        
            if (interval > 20 && wait (interval - 20))
            {
                Time::waitForMillisecondCounter (currentTime + interval);
            }
           // DBG("CV = " << counterValue);
         }
    
    
        counterValue = 0;
//        else
//        {threadShouldExit();}
    }
    

   
    
    
    
        
        
        //DBG("threadShouldExit()" << threadShouldExit());
void Counter::start()
{
    //DBG("?????");
   
    startThread();
    
}

void Counter::stop()
{
    stopThread (2000);
}

void Counter::addListener (Listener* newListener)
{
    listenerList.add (newListener);
}


/** set interval by bpmSlider value */
float Counter::setInterval (int bpm)
{
    interval = (60000 / bpm)/4;
    
    return interval;
}

float Counter::getCounterValue()
{
    return counterValue;
}

float Counter::getInterval()
{
    return interval;
}

void Counter::setPlayState(int state)
{
    DBG("state == " << state);
    //start();
    
    playState = state;
    
}

bool Counter::getState()
{
    return playState;
}
