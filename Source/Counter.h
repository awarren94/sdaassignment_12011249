//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//

#ifndef COUNTER_H_INCLUDED
#define COUNTER_H_INCLUDED

#include "JuceHeader.h"


class Counter: public Thread

{
public:
    /** Constructor */
    Counter();
    
    /** Destructor */
    ~Counter();
    
    
    void run() override;
    
    void start();
    
    void stop();
    
    /** Class for counter listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (uint64 counterValue) = 0;
    };
    
    void addListener (Listener* newListener);
    
    float setInterval (int bpm);
    
    float getCounterValue();
    
    float getInterval();
    
    void setPlayState (int state);
    
    bool getState();
    
private:
    ListenerList<Listener> listenerList;
    int counterValue;
    uint32 startTime;
    uint32 interval;
    
    bool playState;

    
};



#endif /* COUNTER_H_INCLUDED */
