//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/12/2015.
//
//

#ifndef __JuceBasicAudio__Oscillator__
#define __JuceBasicAudio__Oscillator__

#include <stdio.h>
#include "Juceheader.h"

#define MAX_HARMONICS 32


/**
 Class for an oscillator
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     SinOscillator constructor
     */
    Oscillator();
    
    /**
     SinOscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     */
    float setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     */
    float getSample();
    
    /**
     function that provides the execution of the waveshape
     */
    virtual float renderWave (const float currentPhase) = 0;
    
    void octaveSelect(int buttonValue);
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
    float octaveMuliplier;
};





#endif /* defined(__JuceBasicAudio__Oscillator__) */
