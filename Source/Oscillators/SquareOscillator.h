//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#ifndef SQAUREOSCILLATOR_H_INCLUDED
#define SQAUREOSCILLATOR_H_INCLUDED

#include <stdio.h>

#include "Oscillator.h"


class SquareOscillator : public Oscillator
{
public:
    
virtual float renderWave (const float currentPhase) override;
    
    


private:

};

#endif /* SQAUREOSCILLATOR_H_INCLUDED */
