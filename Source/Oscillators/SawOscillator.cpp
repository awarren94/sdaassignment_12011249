//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#include "SawOscillator.h"

float SawOscillator::renderWave (const float currentPhase)
{
 
    fMix = currentPhase;
    
    int count;
    for(count = 0; count < MAX_HARMONICS; count++)
    {
        fMix += harmonic[count].getSample() * (1.0/(count+1));
    }
    return fMix / 2.;
}


