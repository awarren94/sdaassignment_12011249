//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/12/2015.
//
//

#include "Oscillator.h"
#include <cmath>

Oscillator::Oscillator()
{
    octaveMuliplier = 2;
    reset();
}

 Oscillator::~Oscillator()
{
    
}

void  Oscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * (frequency * octaveMuliplier ) / sampleRate );
}

void Oscillator::setNote (int noteNum)
{
    setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

float Oscillator::setAmplitude (float amp)
{
    amplitude = amp;
    return amplitude;
}

void Oscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (100.f);
    setAmplitude (0.f);
}

void Oscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency );//just to update the phaseInc
}

float Oscillator::getSample()
{
    float out = renderWave (phase ) * amplitude ;
    phase = phase + phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}
//
void Oscillator::octaveSelect(int buttonValue)
{
    
    DBG("button value ==" << buttonValue);
//    switch(buttonValue){
//        case  -3:
//            octaveMuliplier = 0.175;
//            break;
//        case -2:
//            octaveMuliplier = 0.25;
//            break;
//        case -1:
//            octaveMuliplier = 0.5;
//            break;
//        case 0:
//            octaveMuliplier = 1.;
//            break;
//        case 1:
//            octaveMuliplier = 2.;
//            break;
//        case 2:
//            octaveMuliplier = 3.;
//            break;
//        default:
//            break;
//    }
    
    DBG("OCTAVE MULITPLIER: " << octaveMuliplier);
    
    
}
