//
//  SawOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#ifndef SAWOSCILLATOR_H_INCLUDED
#define SAWOSCILLATOR_H_INCLUDED

#include <stdio.h>
#include "SinOscillator.h"
#include "Oscillator.h"

class SawOscillator : public Oscillator
{
public:
    virtual float renderWave (const float currentPhase) override;
    

private:
    
    SinOscillator harmonic[MAX_HARMONICS];
    float fMix;
    
    
    
};
#endif /* SAWOSCILLATOR_H_INCLUDED */
