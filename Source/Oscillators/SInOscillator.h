//
//  SInOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 07/12/2015.
//
//

#ifndef SINOSCILLATOR_H_INCLUDED
#define SINOSCILLATOR_H_INCLUDED

#include "JuceHeader.h"
#include "Oscillator.h"

class SinOscillator : public Oscillator
{
public:
    
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    virtual float renderWave (const float currentPhase) override;
    
   
  
    
private:
    float noteAmplitude;
    float phaseIncrement;
    float phase;
    float sampleRate;
    float twoPi = 2 * M_PI;
    float frequency;
};



#endif /* SINOSCILLATOR_H_INCLUDED */
