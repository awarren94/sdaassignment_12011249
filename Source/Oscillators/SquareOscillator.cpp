//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#include "SquareOscillator.h"
              
float SquareOscillator::renderWave (const float currentPhase)
{
    if(currentPhase <= M_PI)
        return 1;
    
    else if(currentPhase > M_PI && currentPhase <= (2 *M_PI))
        return 0;
    
    else
        return false;
}
