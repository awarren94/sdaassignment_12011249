//
//  SInOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 07/12/2015.
//
//

#include "SinOscillator.h"


SinOscillator::SinOscillator()
{
//    phase = 0.f;
//    sampleRate = 44100.;
//    setFrequency (440.f);
//    setAmplitude (0.f);

}

SinOscillator::~SinOscillator()
{
    
}


float SinOscillator::renderWave (const float currentPhase)
{
    return sin (currentPhase);
    
}


