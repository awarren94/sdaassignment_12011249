//
//  SequencerButtonsUI.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//

#include "SequencerButtonsUI.h"
#include "stdio.h"


SequencerButtonsUI::SequencerButtonsUI()
{
    counterPosition = 0;
    
    for(int i = 0; i < amountOfSteps; i++)
    {
        addAndMakeVisible(&onButton[i]);

        onButton[i].setColour(TextButton::buttonColourId, Colours::black);

        onButton[i].setColour(TextButton::buttonOnColourId, Colours::crimson);
        onButton[i].addListener(this);
    }
    
    c = 0;
}

SequencerButtonsUI::~SequencerButtonsUI()
{
}

void SequencerButtonsUI::resized()
{
    for(int i = 0; i < amountOfSteps; i++)
       onButton[i].setBounds((i * getWidth())/amountOfSteps, getHeight()/10, (getWidth()/amountOfSteps) - (getWidth()/400), getHeight ()/ 1.4);
}

//Button Listener
void SequencerButtonsUI::buttonClicked (Button* button)
{
    for(int i = 0; i < amountOfSteps; i++)
    {
        if(button == &onButton[i])
        {
            if(onButton[i].getToggleState() == true)
            {
                onButton[i].setToggleState(false, dontSendNotification);
                DBG(onButton[i].getToggleState());
            }
            else if (onButton[i].getToggleState() == false)
            {
                onButton[i].setToggleState(true, dontSendNotification);
                DBG(onButton[i].getToggleState());
            }
        }
    }
}

void SequencerButtonsUI::highlightStep(int step)
{
    if(step == 0)
        onButton[15].setColour(TextButton::buttonColourId, Colours::black);

    else if(step > 0)
        onButton[step-1].setColour(TextButton::buttonColourId, Colours::black);

    onButton[step].setColour(TextButton::buttonColourId, Colours::darkgrey);
}


bool SequencerButtonsUI::getButtonState(int i)
{
    return onButton[i].getToggleState();
}

const int SequencerButtonsUI::getAmountOfSteps()
{
    return amountOfSteps;
}

void SequencerButtonsUI::resetStepPosition()
{
    for(int i = 0 ; i < 16 ; i++)
        onButton[i].setColour(TextButton::buttonColourId, Colours::black);
}

void SequencerButtonsUI::clearButtons()
{
    for(int i = 0 ; i < 16 ; i++)
        onButton[i].setToggleState(false, dontSendNotification);
}


void SequencerButtonsUI::paint (Graphics& g)
{
    g.setColour (Colours::slategrey);
    
    g.drawLine(onButton[4].getX(),onButton[4].getY() /2, onButton[4].getX(), onButton[4].getHeight() * 1.2, onButton[0].getWidth()/5);
    g.drawLine(onButton[8].getX(),onButton[8].getY() /2, onButton[8].getX(), onButton[8].getHeight() * 1.2, onButton[0].getWidth()/5);
    g.drawLine(onButton[12].getX(),onButton[12].getY() /2, onButton[12].getX(), onButton[12].getHeight() * 1.2, onButton[0].getWidth()/5);
   
}


