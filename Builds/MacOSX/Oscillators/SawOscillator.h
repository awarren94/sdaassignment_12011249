//
//  SawOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#ifndef SAWOSCILLATOR_H_INCLUDED
#define SAWOSCILLATOR_H_INCLUDED

#include "SinOscillator.h"
#include "Oscillator.h"

class SawOscillator : public Oscillator
{
public:
    /** Constructor */
    SawOscillator();
    
    /** Destructor */
    ~SawOscillator();
    
    /** Function that provides the execution of the waveshape */
    virtual float renderWave (const float currentPhase) override;
    

private:
    
    float fMix;
    float bit = 2*M_PI/16 ;
    float main = 2*M_PI;
    
};
#endif /* SAWOSCILLATOR_H_INCLUDED */
