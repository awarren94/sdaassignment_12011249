//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#ifndef SQAUREOSCILLATOR_H_INCLUDED
#define SQAUREOSCILLATOR_H_INCLUDED


#include "Oscillator.h"
#include "SinOscillator.h"



class SquareOscillator : public Oscillator
{
public:
    
    SquareOscillator();
    
    ~SquareOscillator();

    void setFrequency (float freq) override;
    
    void setAmplitude (float amp) override;
    
    void setSampleRate (float sr) override;
       
    float renderWave(const float currentPhase) override;
    
private:
    
    SinOscillator oscillator[32];
    float mixharmonic;
    
};



#endif /* SQAUREOSCILLATOR_H_INCLUDED */
