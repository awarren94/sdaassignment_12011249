//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#include "SawOscillator.h"


SawOscillator::SawOscillator()
{
    
}

SawOscillator::~SawOscillator()
{
    
}
float SawOscillator::renderWave (const float currentPhase)
{
// 
      fMix = currentPhase;
    
//    int count;
//    for(count = 0; count < MAX_HARMONICS; count++)
//    {
//        fMix += harmonic[count].getSample() * (1.0/(count+1));
//    }
//    return fMix / 2.;
//    fMix *= 0.995;
//    return fMix;
//    
//    
    if(fMix <= (main - (15*bit)))
        return 1;
    else if(fMix > (main - (15*bit))  && fMix <= (main - (14*bit)))
        return 0.875;
    else if(fMix > (main - (14*bit))  && fMix <= (main - (13*bit)))
        return 0.75;
    else if(fMix > (main - (13*bit))  && fMix <= (main - (12*bit)))
        return 0.625;
    else if(fMix > (main - (12*bit))  && fMix <= (main - (11*bit)))
        return 0.5;
    else if(fMix > (main - (11*bit))  && fMix <= (main - (10*bit)))
        return 0.375;
    else if(fMix > (main - (10*bit))  && fMix <= (main - (9*bit)))
        return 0.25;
    else if(fMix > (main - (9*bit))  && fMix <= (main - (8*bit)))
        return 0.125;
    else if(fMix > (main - (8*bit))  && fMix <= (main - (7*bit)))
        return 0;
    else if(fMix > (main - (7*bit))  && fMix <= (main - (6*bit)))
        return -0.125;
    else if(fMix > (main - (6*bit))  && fMix <= (main - (5*bit)))
        return -0.25;
    else if(fMix > (main - (5*bit))  && fMix <= (main - (4*bit)))
        return -0.375;
    else if(fMix > (main - (4*bit))  && fMix <= (main - (3*bit)))
        return -0.5;
    else if(fMix > (main - (3*bit))  && fMix <= (main - (2*bit)))
        return -0.625;
    else if(fMix > (main - (2*bit))  && fMix <= (main - bit))
        return -0.75;
    else if(fMix > (main - bit)  && fMix <= main )
        return -1;
    else
        return false;
}
