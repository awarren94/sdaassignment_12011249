//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/12/2015.
//
//

#ifndef OSCILLATOR_H_INCLUDED
#define OSCILLATOR_H_INCLUDED

#include <stdio.h>
#include "Juceheader.h"


#define MAX_HARMONICS 32


/**
 Class for an oscillator
 */

class Oscillator : public Synthesiser
{
public:
    //==============================================================================
    /**
     Constructor
     */
    Oscillator();
    
    /**
     Destructor
     */
    virtual ~Oscillator();
    
    /**
     Sets the frequency of the oscillator
     @param sets the note frequency
     */
    virtual void setFrequency (float freq);
    
    /**
     Sets frequency using a midi note number
     @param sets the note note from the note number
     */
    virtual void setNote (int noteNum);
    
    /**
     Sets the amplitude of the oscillator
     @param New amplitude
     */
    virtual void setAmplitude (float amp);
    
    void stop();
    
    /**
     Resets the oscillator
     */
    void reset();
    
    /**
     Sets the sample rate
     @param sets the sample rate
     */
    virtual void setSampleRate (float sr);
    
    /**
     Returns the next sample
     @return the next sample
     */
    float getSample();
    
    /**
     Pure virtual function that provides the execution of the waveshape to be overriden
     */
    virtual float renderWave (const float currentPhase) = 0;
    
    float getFrequency();
   

    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
    float octaveMuliplier;
   // OctaveButtons octaveButtons;
  
   
    
};





#endif /* OSCILLATOR_H_INCLUDED */
