//
//  SInOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 07/12/2015.
//
//

#include "SinOscillator.h"


SinOscillator::SinOscillator()
{
}

SinOscillator::~SinOscillator()
{
}


float SinOscillator::renderWave (const float currentPhase)
{
    return sin (currentPhase);
}


