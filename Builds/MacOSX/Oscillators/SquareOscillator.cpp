//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 16/12/2015.
//
//

#include "SquareOscillator.h"


SquareOscillator:: SquareOscillator()
{
    setSampleRate(44100);
    
}

SquareOscillator:: ~SquareOscillator()
{
    
}

void SquareOscillator::setFrequency(float freq)
{
    Oscillator::setFrequency (freq);
    for (int i = 0; i < 32; i++)
    {
        oscillator[i].setFrequency (freq * i);
    }

}


void SquareOscillator::setAmplitude (float amp)
{
    Oscillator::setAmplitude (amp);
    for (int i = 0; i < 32; i++)
    {
        oscillator[i].setAmplitude (amp);
    }
}

void SquareOscillator::setSampleRate (float sr)
{
    Oscillator::setSampleRate (sr);
    for (int i = 0; i < 32; i++)
    {
        oscillator[i].setSampleRate (sr);
    }
}


float SquareOscillator::renderWave(const float currentPhase)
{
    
    float mixharmonic = 0;
  
    for (int i = 0; i < 32; i++)
    {
        if (i  & 1)
        {
            mixharmonic += oscillator[i].getSample() / (i * 2 - 1);
        }
    }

    return mixharmonic;
    
}



//////////////////////////////////////////////////////////


//float SquareOscillator::renderWave (const float currentPhase)
//{
//    if(currentPhase <= M_PI)
//        return 1;
//
//    else if(currentPhase > M_PI && currentPhase <= (2 *M_PI))
//        return 0;
//
//    else
//        return false;
//}

//void SquareOscillator::setNote (int noteNum)
//{
//    const float frequency = 440.f * pow (2, (noteNum - 69) / 12.0);
//    Oscillator::setFrequency (frequency);
//    for (int i = 0; i < 32; i++)
//    {
//        oscillator[i].setFrequency (frequency * i);
//    }
//}


