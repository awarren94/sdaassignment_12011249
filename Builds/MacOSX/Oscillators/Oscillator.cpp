//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Andrew Warren on 11/12/2015.
//
//

#include "Oscillator.h"
#include <cmath>

Oscillator::Oscillator()
{
    reset();
}

 Oscillator::~Oscillator()
{
}

void  Oscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * frequency / sampleRate );
    //this->getFrequency();
}

float Oscillator::getFrequency()
{
    return frequency;
}

void Oscillator::setNote (int noteNum)
{
    setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void Oscillator::setAmplitude (float amp)
{
    amplitude = amp;
}

void Oscillator::stop()
{
    amplitude = 0;
}
void Oscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (100.f);
    setAmplitude (0.f);
}

void Oscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency );//just to update the phaseInc
}

float Oscillator::getSample()
{
    
    float out = renderWave (phase ) * amplitude ;
    phase = phase + phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}





