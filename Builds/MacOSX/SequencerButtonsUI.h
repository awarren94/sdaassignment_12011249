//
//  SequencerButtonsUI.h
//  JuceBasicAudio
//
//  Created by Andrew Warren on 02/12/2015.
//
//

#ifndef SEQUENCERBUTTONSUI_H_INCLUDED
#define SEQUENCERBUTTONSUI_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Counter.h"

class SequencerButtonsUI :  public Component,
                            public Button::Listener

{
public:

    SequencerButtonsUI();
    
    ~SequencerButtonsUI();
    
    void resized();
    
    //Button Listener
    void buttonClicked (Button* button);
    
    const int getAmountOfSteps();
    
    void highlightStep(int step);
    
    bool getButtonState(int i);
    
    void resetStepPosition();
    
    void clearButtons();

    void paint (Graphics& g);
    
    
private:

    static const int amountOfSteps = 16;
    TextButton onButton[amountOfSteps];
    ToggleButton startButton;
    juce::ArrowButton::ButtonState buttonDown;
    int c ;
    Atomic<int> counterPosition;
    bool startState;
   // Audio vaudio;
};



#endif /* SEQUENCERBUTTONSUI_H_INCLUDED */
